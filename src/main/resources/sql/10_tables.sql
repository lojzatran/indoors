DROP TABLE IF EXISTS zone;
CREATE TABLE zone (
  id          BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name        VARCHAR(255)                      NOT NULL,
  description VARCHAR(1024) NULL,
  beacon_id   BIGINT        NULL,
  zone_size   VARCHAR(255)  NULL
);

DROP TABLE IF EXISTS beacon;
CREATE TABLE beacon (
  id                BIGINT AUTO_INCREMENT PRIMARY KEY     NOT NULL,
  name              VARCHAR(255)                          NOT NULL,
  description       VARCHAR(1024)                         NULL,
  vendor            VARCHAR(255)                          NULL,
  uuid              VARCHAR(255)                          NULL,
  major             INT                                   NULL,
  minor             INT                                   NULL,
  tx_power          INT                                   NULL,
  installation_date TIMESTAMP NULL,
  lat               INT       NULL,
  lon               INT       NULL
);