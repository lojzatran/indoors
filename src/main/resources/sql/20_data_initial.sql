INSERT INTO indoors.beacon (id, description, lat, lon, installation_date, major, minor, name, tx_power, uuid, vendor)
VALUES
  (1, 'beacon description', 44, 66, '2015-09-14 21:25:12', 10, 1, 'Beacon 1', 10, 'UUID', 'VENDOR_A');


INSERT INTO indoors.zone (id, beacon_id, description, name, zone_size)
VALUES (1, NULL, 'zone description', 'large zone', 'LARGE');
