/**
 * Service for operations with zone entity
 */
define([], function ZoneServiceModule() {
    "use strict";

    return ["$http", "$q", function ($http, $q) {

        return {
            getAllZones: function getZones() {
                var deferred = $q.defer();
                $http.get('/zones').success(function (zones) {
                    deferred.resolve(zones);
                });
                return deferred.promise;
            },
            save: function save(zone) {
                return $http.post('/zones', zone);
            },
            update: function update(zone) {
                return $http.put('/zones/' + zone.id, zone);
            },
            delete: function (zoneId) {
                return $http.delete('/zones/' + zoneId);
            },
            getZoneById: function getZoneById(zoneId) {
                var deferred = $q.defer();
                $http.get('/zones/' + zoneId).success(function (zone) {
                    deferred.resolve(zone);
                });
                return deferred.promise;
            }
        }
    }];
});