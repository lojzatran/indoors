/**
 *  Angular controller for zone modal window
 */
define([], function ZoneModalWindowControllerModule() {
    "use strict";

    return ['$scope', 'zoneService', 'zone', 'beacons',
        function ($scope, zoneService, zone, beacons) {
            $scope.zone = zone;
            $scope.beacons = beacons;
            $scope.cancel = function () {
                $scope.$dismiss();
            };
            $scope.save = function () {
                zoneService.save($scope.zone).success(function () {
                    $scope.$close(true);
                });
            };

            $scope.update = function () {
                zoneService.update($scope.zone).success(function () {
                    $scope.$close(true);
                });
            };

            $scope.open = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.opened = true;
            };
        }];
});