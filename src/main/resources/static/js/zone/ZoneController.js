/**
 *  Angular controller for zone entity
 */
define([], function ZoneControllerModule() {
    "use strict";

    return ["zones", "zoneService", function ZoneController(zones, zoneService) {
        var thisController = this;
        thisController.zones = zones;

        thisController.delete = function (zoneId) {
            zoneService.delete(zoneId).success(function () {
                // we remove it on client side as well
                thisController.zones = thisController.zones.filter(function (zone) {
                    return zone.id !== zoneId;
                });
            });
        };
    }];
});