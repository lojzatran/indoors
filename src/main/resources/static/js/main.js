require(["angular", "ui.router", "ui.bootstrap",
        "AppStatesConfig",
        "beacon/BeaconController", "beacon/BeaconModalWindowController",
        "zone/ZoneController", "zone/ZoneModalWindowController",
        "beacon/BeaconService", "zone/ZoneService",
        "directive/ClickConfirmDirective", "directive/TitleDirective"],
    function (angular, router, bootstrap,
              AppStatesConfig,
              BeaconController, BeaconModalWindowController,
              ZoneController, ZoneModalWindowController,
              beaconService, zoneService,
              ClickConfirmDirective, TitleDirective) {
        "use strict";

        var app = angular.module('indoorsDemo', ["ui.router", "ui.bootstrap"]);

        app.config(AppStatesConfig);

        // controllers
        app.controller('BeaconController', BeaconController);
        app.controller('BeaconModalWindowController', BeaconModalWindowController);
        app.controller('ZoneController', ZoneController);
        app.controller('ZoneModalWindowController', ZoneModalWindowController);

        // services
        app.service('beaconService', beaconService);
        app.service('zoneService', zoneService);

        // directives
        app.directive('indClickConfirm', ClickConfirmDirective);
        app.directive('indTitle', TitleDirective);

        angular.element(document).ready(function () {
            angular.bootstrap(document, ['indoorsDemo']);
        });
    }
);