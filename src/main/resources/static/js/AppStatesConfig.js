/**
 * Configure different ui-router states
 */
define([], function AppStatesConfigModule() {
    "use strict";
    var $openedModal;

    return ["$stateProvider", "$urlRouterProvider",

        function configureAppStates($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state("beacons", {
                    url: "/beacons",
                    controller: "BeaconController as beaconController",
                    templateUrl: "/templates/beacon/list.tpl.html",
                    resolve: {
                        beacons: ["beaconService", function (beaconService) {
                            return beaconService.getAllBeacons();
                        }]
                    }
                }).
                state("beacons.create", {
                    url: "/create",
                    onEnter: ['$state', '$modal',
                        function ($state, $modal) {
                            $openedModal = $modal.open({
                                templateUrl: "/templates/beacon/create.tpl.html",
                                resolve: {
                                    beacon: function () {
                                        return {installationDate: new Date(), vendor: "VENDOR_A"}
                                    }
                                },
                                controller: "BeaconModalWindowController",
                                animation: false
                            });
                            $openedModal.result.finally(function () {
                                $state.go('^', null, {reload: true});
                            });
                        }
                    ],
                    onExit: [function () {
                        if ($openedModal) {
                            $openedModal.close("");
                        }
                    }]
                }).
                state("beacons.edit", {
                    url: "/:beaconId/edit",
                    onEnter: ['$state', '$modal', '$stateParams',
                        function ($state, $modal, $stateParams) {
                            var beaconId = $stateParams.beaconId;
                            $openedModal = $modal.open({
                                templateUrl: "/templates/beacon/edit.tpl.html",
                                resolve: {
                                    beacon: ["beaconService", function (beaconService) {
                                        return beaconService.getBeaconById(beaconId);
                                    }]
                                },
                                controller: "BeaconModalWindowController",
                                animation: false
                            });
                            $openedModal.result.finally(function () {
                                $state.go('^', null, {reload: true});
                            });
                        }
                    ],
                    onExit: [function () {
                        if ($openedModal) {
                            $openedModal.close("");
                        }
                    }]
                }).
                state("zones", {
                    url: "/zones",
                    controller: "ZoneController as zoneController",
                    templateUrl: "/templates/zone/list.tpl.html",
                    resolve: {
                        zones: ["zoneService", function (zoneService) {
                            return zoneService.getAllZones();
                        }]
                    }
                }).
                state("zones.create", {
                    url: "/create",
                    onEnter: ['$state', '$modal',
                        function ($state, $modal) {
                            $openedModal = $modal.open({
                                templateUrl: "/templates/zone/create.tpl.html",
                                resolve: {
                                    zone: function () {
                                        return {size: 'LARGE'}
                                    },
                                    beacons: ["beaconService", function (beaconService) {
                                        return beaconService.getAllBeacons();
                                    }]
                                },
                                controller: "ZoneModalWindowController",
                                animation: false
                            });
                            $openedModal.result.finally(function () {
                                $state.go('^', null, {reload: true});
                            });
                        }
                    ],
                    onExit: [function () {
                        if ($openedModal) {
                            $openedModal.close("");
                        }
                    }]
                }).
                state("zones.edit", {
                    url: "/:zoneId/edit",
                    onEnter: ['$state', '$modal', '$stateParams',
                        function ($state, $modal, $stateParams) {
                            var zoneId = $stateParams.zoneId;
                            $openedModal = $modal.open({
                                templateUrl: "/templates/zone/edit.tpl.html",
                                resolve: {
                                    zone: ["zoneService", function (zoneService) {
                                        return zoneService.getZoneById(zoneId);
                                    }],
                                    beacons: ["beaconService", function (beaconService) {
                                        return beaconService.getAllBeacons();
                                    }]
                                },
                                controller: "ZoneModalWindowController",
                                animation: false
                            });
                            $openedModal.result.finally(function () {
                                $state.go('^', null, {reload: true});
                            });
                        }
                    ],
                    onExit: [function () {
                        if ($openedModal) {
                            $openedModal.close("");
                        }
                    }]
                });

            // default state
            $urlRouterProvider.otherwise(function () {
                console.log("can't find any suitable state - use default");
                return "/zones";
            });
        }];
});