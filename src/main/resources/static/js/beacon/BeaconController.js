/**
 * Angular controller for beacon page
 */
define([], function BeaconControllerModule() {
    "use strict";

    return ["beacons", "beaconService",
        function BeaconController(beacons, beaconService) {
            var thisController = this;
            thisController.beacons = beacons;

            thisController.delete = function (beaconId) {
                beaconService.delete(beaconId).success(function () {
                    // we remove it on client side as well
                    thisController.beacons = thisController.beacons.filter(function (beacon) {
                        return beacon.id !== beaconId;
                    });
                });
            };
        }
    ];
});