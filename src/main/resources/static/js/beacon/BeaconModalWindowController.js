/**
 * Angular controller for beacon modal window
 */
define([], function BeaconModalWindowControllerModule() {
    "use strict";

    return ['$scope', 'beaconService', 'beacon',
        function ($scope, beaconService, beacon) {
            $scope.beacon = beacon;
            $scope.cancel = function () {
                $scope.$dismiss();
            };
            $scope.save = function () {
                beaconService.save($scope.beacon).success(function () {
                    $scope.$close(true);
                });
            };

            $scope.update = function () {
                beaconService.update($scope.beacon).success(function () {
                    $scope.$close(true);
                });
            };

            $scope.open = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.opened = true;
            };
        }];
});