/**
 * Service for operations with beacon entity
 */
define([], function BeaconServiceModule() {
    "use strict";

    return ["$http", "$q", function ($http, $q) {
        return {
            getBeaconById: function getBeaconById(beaconId) {
                var deferred = $q.defer();
                $http.get('/beacons/' + beaconId).success(function (beacon) {
                    deferred.resolve(beacon);
                });
                return deferred.promise;
            },
            getAllBeacons: function getAllBeacons() {
                var deferred = $q.defer();
                $http.get('/beacons').success(function (beacons) {
                    deferred.resolve(beacons);
                });
                return deferred.promise;
            },
            update: function update(beacon) {
                return $http.put('/beacons/' + beacon.id, beacon);
            },
            delete: function (beaconId) {
                return $http.delete('/beacons/' + beaconId);
            },
            save: function save(beacon) {
                return $http.post('/beacons', beacon);
            }
        }
    }];
});