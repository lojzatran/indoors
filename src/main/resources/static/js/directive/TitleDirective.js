/**
 * Set title of the page
 */
define([], function TitleDirectiveModule() {
    "use strict";

    return ['$rootScope', '$timeout',
        function () {
            return {
                transclude: true,
                link: function (scope, element, attrs, controllers, transcludeFn) {
                    var titleEl = document.getElementsByTagName("title")[0];
                    titleEl.innerHTML = transcludeFn().text();
                }
            };
        }
    ];
});