/**
 * Display confirm window when a link or button is clicked
 */
define([], function ClickConfirmDirectiveModule() {
    "use strict";

    return [function () {
        return {
            link: function (scope, element, attr) {
                var msg = attr.indConfirmClick || "Are you sure?";
                var clickAction = attr.indOnConfirm;
                element.bind('click', function (event) {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction);
                    }
                });
            }
        };
    }];
});