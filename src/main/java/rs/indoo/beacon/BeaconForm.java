package rs.indoo.beacon;

import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Form for creating and updating
 * {@link Beacon} from the client
 *
 * @author lojzatran
 */
public class BeaconForm {
    private static final Logger LOG = LoggerFactory.getLogger(BeaconForm.class);

    @NotEmpty
    private String name;

    private String description;

    @NotNull
    private Vendor vendor;

    private String uuid;

    private Integer major;

    private Integer minor;

    private Integer txPower;

    private Date installationDate;

    private Integer lat;

    private Integer lon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getMajor() {
        return major;
    }

    public void setMajor(Integer major) {
        this.major = major;
    }

    public Integer getMinor() {
        return minor;
    }

    public void setMinor(Integer minor) {
        this.minor = minor;
    }

    public Integer getTxPower() {
        return txPower;
    }

    public void setTxPower(Integer txPower) {
        this.txPower = txPower;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }

    public Integer getLat() {
        return lat;
    }

    public void setLat(Integer lat) {
        this.lat = lat;
    }

    public Integer getLon() {
        return lon;
    }

    public void setLon(Integer lon) {
        this.lon = lon;
    }

    public Beacon createBeaconDomain() {
        Beacon beacon = new Beacon();
        updateBeaconDomain(beacon);
        return beacon;
    }

    public void updateBeaconDomain(Beacon beacon) {
        beacon.setName(name);
        beacon.setVendor(vendor);
        beacon.setDescription(description);
        beacon.setInstallationDate(installationDate);
        beacon.setUuid(uuid);
        beacon.setMajor(major);
        beacon.setMinor(minor);
        beacon.setTxPower(txPower);
        if (lat != null && lon != null) {
            beacon.setGeoLocation(new GeoLocation(lat, lon));
        }
    }
}