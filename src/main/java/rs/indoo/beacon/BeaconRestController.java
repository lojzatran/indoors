package rs.indoo.beacon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Rest controller for beacon
 *
 * @author lojzatran
 */
@RestController
@RequestMapping("/beacons")
public class BeaconRestController {
    private static final Logger LOG = LoggerFactory.getLogger(BeaconRestController.class);

    @Autowired
    private BeaconRepository beaconRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<Beacon> list() {
        List<Beacon> beaconList = beaconRepository.findAll();
        return beaconList;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Beacon> save(@Valid @RequestBody BeaconForm beaconForm,
                                       BindingResult result, UriComponentsBuilder builder) {
        if (result.hasErrors()) {
            LOG.info("Can't create beacon entity: {}", result.getAllErrors());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            Beacon beacon = beaconForm.createBeaconDomain();
            beacon = beaconRepository.save(beacon);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(builder.path("/beacons/{id}")
                    .buildAndExpand(beacon.getId().toString()).toUri());
            return new ResponseEntity<>(beacon, headers, HttpStatus.CREATED);
        }
    }

    @RequestMapping(value = "/{beaconId}", method = RequestMethod.GET)
    public ResponseEntity<Beacon> get(@PathVariable("beaconId") Long beaconId) {
        Optional<Beacon> beaconOpt = beaconRepository.findById(beaconId);
        if (beaconOpt.isPresent()) {
            HttpHeaders headers = new HttpHeaders();
            return new ResponseEntity<>(beaconOpt.get(), headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{beaconId}", method = RequestMethod.PUT)
    public ResponseEntity<Beacon> update(@PathVariable("beaconId") Long beaconId,
                                         @Valid @RequestBody BeaconForm beaconForm) {
        Optional<Beacon> beaconOpt = beaconRepository.findById(beaconId);
        if (beaconOpt.isPresent()) {
            Beacon beacon = beaconOpt.get();
            beaconForm.updateBeaconDomain(beacon);
            beaconRepository.save(beacon);
            HttpHeaders headers = new HttpHeaders();
            return new ResponseEntity<>(beacon, headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{beaconId}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("beaconId") Long beaconId) {
        Optional<Beacon> beaconOpt = beaconRepository.findById(beaconId);
        if (beaconOpt.isPresent()) {
            beaconRepository.delete(beaconId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}