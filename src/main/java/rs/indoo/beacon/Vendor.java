package rs.indoo.beacon;

/**
 * Beacon vendor enum
 *
 * @author lojzatran
 */
public enum Vendor {
    VENDOR_A, VENDOR_B, OTHER
}
