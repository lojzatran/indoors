package rs.indoo.beacon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.Date;

/**
 * Beacon entity
 *
 * @author lojzatran
 */
@Entity
@Table(name = "beacon")
public class Beacon {
    private static final Logger LOG = LoggerFactory.getLogger(Beacon.class);

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    @Enumerated(EnumType.STRING)
    private Vendor vendor;

    @Column
    private String uuid;

    @Column
    private Integer major;

    @Column
    private Integer minor;

    @Column
    private Integer txPower;

    @Column
    private Date installationDate;

    @Embedded
    private GeoLocation geoLocation;

    public Beacon() {
    }

    public Beacon(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getMajor() {
        return major;
    }

    public void setMajor(Integer major) {
        this.major = major;
    }

    public Integer getMinor() {
        return minor;
    }

    public void setMinor(Integer minor) {
        this.minor = minor;
    }

    public Integer getTxPower() {
        return txPower;
    }

    public void setTxPower(Integer txPower) {
        this.txPower = txPower;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public Integer getLat() {
        return this.geoLocation == null ? null : this.geoLocation.getLat();
    }

    public Integer getLon() {
        return this.geoLocation == null ? null : this.geoLocation.getLon();
    }
}

@Embeddable
class GeoLocation {
    private Integer lat;

    private Integer lon;

    public GeoLocation() {
    }

    public GeoLocation(Integer lat, Integer lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public Integer getLat() {
        return lat;
    }

    public void setLat(Integer lat) {
        this.lat = lat;
    }

    public Integer getLon() {
        return lon;
    }

    public void setLon(Integer lon) {
        this.lon = lon;
    }
}