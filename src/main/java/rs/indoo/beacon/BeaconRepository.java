package rs.indoo.beacon;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * DB repository for beacon
 *
 * @author lojzatran
 */
@Repository
public interface BeaconRepository extends JpaRepository<Beacon, Long> {

    Optional<Beacon> findById(Long id);
}