package rs.indoo.zone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Rest controller for {@link Zone}
 *
 * @author lojzatran
 */
@RestController
@RequestMapping("/zones")
public class ZoneRestController {
    private static final Logger LOG = LoggerFactory.getLogger(ZoneRestController.class);

    @Autowired
    private ZoneRepository zoneRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<Zone> list() {
        List<Zone> zoneList = zoneRepository.findAll();
        return zoneList;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Zone> save(@Valid @RequestBody ZoneForm zoneForm,
                                     BindingResult result, UriComponentsBuilder builder) {
        if (result.hasErrors()) {
            LOG.info("Can't create zone entity: {}", result.getAllErrors());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            Zone zone = zoneForm.createZoneDomain();
            zone = zoneRepository.save(zone);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(builder.path("/zones/{id}")
                    .buildAndExpand(zone.getId().toString()).toUri());
            return new ResponseEntity<>(zone, headers, HttpStatus.CREATED);
        }
    }

    @RequestMapping(value = "/{zoneId}", method = RequestMethod.GET)
    public ResponseEntity<Zone> get(@PathVariable("zoneId") Long zoneId) {
        Optional<Zone> zoneOpt = zoneRepository.findById(zoneId);
        if (zoneOpt.isPresent()) {
            HttpHeaders headers = new HttpHeaders();
            return new ResponseEntity<>(zoneOpt.get(), headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{zoneId}", method = RequestMethod.PUT)
    public ResponseEntity<Zone> update(@PathVariable("zoneId") Long zoneId,
                                       @Valid @RequestBody ZoneForm zoneForm) {
        Optional<Zone> zoneOpt = zoneRepository.findById(zoneId);
        if (zoneOpt.isPresent()) {
            Zone zone = zoneOpt.get();
            zoneForm.updateZoneDomain(zone);
            zoneRepository.save(zone);
            HttpHeaders headers = new HttpHeaders();
            return new ResponseEntity<>(zone, headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{zoneId}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("zoneId") Long zoneId) {
        Optional<Zone> zoneOpt = zoneRepository.findById(zoneId);
        if (zoneOpt.isPresent()) {
            zoneRepository.delete(zoneId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}