package rs.indoo.zone;

/**
 * Zone size enum
 *
 * @author lojzatran
 */
public enum ZoneSize {
    LARGE, MEDIUM, SMALL
}
