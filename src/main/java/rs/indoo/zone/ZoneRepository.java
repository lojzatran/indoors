package rs.indoo.zone;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * DB repository for {@link Zone}
 *
 * @author lojzatran
 */
@Repository
public interface ZoneRepository extends JpaRepository<Zone, Long> {

    Optional<Zone> findById(long zoneId);
}
