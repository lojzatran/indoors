package rs.indoo.zone;

import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rs.indoo.beacon.Beacon;

/**
 * Form for creating and updating
 * {@link Zone} from the client
 *
 * @author lojzatran
 */
public class ZoneForm {
    private static final Logger LOG = LoggerFactory.getLogger(ZoneForm.class);

    @NotBlank
    private String name;

    private String description;

    private Long beaconId;

    private ZoneSize size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(Long beaconId) {
        this.beaconId = beaconId;
    }

    public ZoneSize getSize() {
        return size;
    }

    public void setSize(ZoneSize size) {
        this.size = size;
    }

    public Zone createZoneDomain() {
        Zone zone = new Zone();
        updateZoneDomain(zone);
        return zone;
    }

    public void updateZoneDomain(Zone zone) {
        zone.setName(name);
        zone.setDescription(description);
        if (beaconId != null) {
            zone.setBeacon(new Beacon(beaconId));
        }
        zone.setSize(size);
    }
}