package rs.indoo.zone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rs.indoo.beacon.Beacon;

import javax.persistence.*;

/**
 * A zone which contains a beacon
 *
 * @author lojzatran
 */
@Entity
@Table(name = "zone")
public class Zone {
    private static final Logger LOG = LoggerFactory.getLogger(Zone.class);

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private String description;

    @OneToOne
    @JoinColumn(name = "beacon_id", unique = true)
    private Beacon beacon;

    @Column(name = "beacon_id", insertable = false, updatable = false)
    private Long beaconId;

    @Column(name = "zone_size")
    @Enumerated(EnumType.STRING)
    private ZoneSize size;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Beacon getBeacon() {
        return beacon;
    }

    public void setBeacon(Beacon beacon) {
        this.beacon = beacon;
    }

    public ZoneSize getSize() {
        return size;
    }

    public void setSize(ZoneSize size) {
        this.size = size;
    }

    public Long getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(Long beaconId) {
        this.beaconId = beaconId;
    }
}