package rs.indoo.home;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * This controller returns the page to start the AngularJS app
 *
 * @author lojzatran
 */
@Controller
public class HomePageController {
    private static final Logger LOG = LoggerFactory.getLogger(HomePageController.class);

    @RequestMapping("/")
    public String getIndexPage() {
        return "index";
    }
}