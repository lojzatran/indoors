package rs.indoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndoorsApplication {

    public static void main(String[] args) {
        SpringApplication.run(IndoorsApplication.class, args);
    }

}
