package rs.indoo

import org.springframework.http.HttpStatus
import org.springframework.validation.BindingResult
import org.springframework.web.util.UriComponentsBuilder
import rs.indoo.beacon.*
import spock.lang.Specification

/**
 * {@link BeaconRestController} specs
 *
 * @author lojzatran
 */
class BeaconRestControllerSpec extends Specification {

    def "when user call list method on controller, it returns all beacons"() {
        setup: "we need to mock a beacon repository which will contain test list of beacons"
        def beaconList = Arrays.asList(new Beacon(1), new Beacon(2))
        BeaconRepository repository = [findAll: {
            return beaconList
        }] as BeaconRepository

        and: "we add this mock to the controller"
        BeaconRestController beaconController = new BeaconRestController()
        beaconController.beaconRepository = repository

        when: "we call list() method on beaconController"
        def result = beaconController.list()

        then: "it returns the test list of beacons"
        result == beaconList
    }

    def "when user fills in the beacon form, it creates a new entity and returns 200 code"() {
        setup: "we need to mock a beacon repository which will save the new beacon into our testing variable"
        long testId = new Random().nextLong()
        Beacon beaconToTest = null
        BeaconRepository repository = [save: { Beacon beacon ->
            beacon.setId(testId)
            beaconToTest = beacon
            return beacon
        }] as BeaconRepository

        and: "we add this mock to the controller"
        BeaconRestController beaconController = new BeaconRestController()
        beaconController.beaconRepository = repository

        and: "we also need to mock binding result"
        BindingResult bindingResult = [hasErrors: { return false }] as BindingResult

        when: "we fake sending form from client to the beaconController"
        def uuid = UUID.randomUUID().toString()
        def result = beaconController.save(
                new BeaconForm(name: "testName", description: "testDecription",
                        vendor: Vendor.OTHER, uuid: uuid, major: 1, minor: 1,
                        txPower: 1, installationDate: new Date(), lat: 1, lon: 1),
                bindingResult, UriComponentsBuilder.newInstance())

        then: "it returns correct code, correct path and create beacon entity from form"
        HttpStatus.CREATED == result.statusCode
        '/beacons/' + testId == result.getHeaders().getLocation().path
        beaconToTest.uuid == uuid
    }

    def "when user is deleted, the server should return 204 code"() {
        setup: "we need to mock a beacon repository which will save the new beacon into our testing variable"
        def testBeaconToDelete = new Beacon()
        BeaconRepository repository = [findById: { Long id ->
            return Optional.of(testBeaconToDelete)
        }, delete                              : { Long id ->
        }] as BeaconRepository

        BeaconRestController beaconController = new BeaconRestController()
        beaconController.beaconRepository = repository

        when: "delete request comes from the client"
        def result = beaconController.delete(1)

        then:
        HttpStatus.NO_CONTENT == result.getStatusCode()
    }
}
