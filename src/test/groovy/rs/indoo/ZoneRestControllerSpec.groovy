package rs.indoo

import org.springframework.http.HttpStatus
import org.springframework.validation.BindingResult
import org.springframework.web.util.UriComponentsBuilder
import rs.indoo.zone.*
import spock.lang.Specification

/**
 * {@link ZoneRestController} specs
 *
 * @author lojzatran
 */
class ZoneRestControllerSpec extends Specification {

    def "when user call list method on controller, it returns all zones"() {
        setup: "we need to mock a zone repository which will contain test list of zones"
        def zoneList = Arrays.asList(new Zone(id: 1), new Zone(id: 2))
        ZoneRepository repository = [findAll: {
            return zoneList
        }] as ZoneRepository

        and: "we add this mock to the controller"
        ZoneRestController zoneController = new ZoneRestController()
        zoneController.zoneRepository = repository

        when: "we call list() method on zoneController"
        def result = zoneController.list()

        then: "it returns the test list of zones"
        result == zoneList
    }

    def "when user fills in the zone form, it creates a new entity and returns 200 code"() {
        setup: "we need to mock a zone repository which will save the new zone into our testing variable"
        long zoneTestId = new Random().nextLong()
        long beaconTestId = new Random().nextLong()
        Zone zoneToTest = null
        ZoneRepository repository = [save: { Zone zone ->
            zone.setId(zoneTestId)
            zoneToTest = zone
            return zone
        }] as ZoneRepository

        and: "we add this mock to the controller"
        ZoneRestController zoneController = new ZoneRestController()
        zoneController.zoneRepository = repository

        and: "we also need to mock binding result"
        BindingResult bindingResult = [hasErrors: { return false }] as BindingResult

        when: "we fake sending form from client to the zoneController"
        def result = zoneController.save(
                new ZoneForm(name: "testName", description: "testDecription", beaconId: beaconTestId, size: ZoneSize.LARGE),
                bindingResult, UriComponentsBuilder.newInstance())

        then: "it returns correct code, correct path and create zone entity from form"
        HttpStatus.CREATED == result.statusCode
        '/zones/' + zoneTestId == result.getHeaders().getLocation().path
        zoneToTest.beacon.id == beaconTestId
    }

    def "when user is deleted, the server should return 204 code"() {
        setup: "we need to mock a zone repository which will save the new zone into our testing variable"
        def testZoneToDelete = new Zone()
        ZoneRepository repository = [findById: { Long id ->
            return Optional.of(testZoneToDelete)
        }, delete                            : { Long id ->
        }] as ZoneRepository

        ZoneRestController zoneController = new ZoneRestController()
        zoneController.zoneRepository = repository

        when: "delete request comes from the client"
        def result = zoneController.delete(1)

        then:
        HttpStatus.NO_CONTENT == result.getStatusCode()
    }
}
